package yuu.command

import cats.effect.IO
import discord4j.core.event.domain.interaction.ChatInputInteractionEvent
import discord4j.discordjson.json.ApplicationCommandRequest
import yuu.Command
import yuu.implicits._

object PingCmd extends Command {
  override val command: ApplicationCommandRequest =
    ApplicationCommandRequest.builder()
      .name("ping")
      .description("Checks if the bot is up")
      .build()

  override def handle(event: ChatInputInteractionEvent): fs2.Stream[IO, Void] = {
    event.reply("Pong!")
      .asStream()
  }
}
