package yuu.command

import cats.effect.IO
import com.typesafe.scalalogging.LazyLogging
import discord4j.core.event.domain.interaction.ChatInputInteractionEvent
import discord4j.discordjson.json.ApplicationCommandRequest
import fs2.Stream
import io.circe.generic.AutoDerivation
import sttp.client3._
import sttp.client3.armeria.cats.ArmeriaCatsBackend
import sttp.tapir.client.sttp.SttpClientInterpreter
import sttp.tapir.generic.auto.schemaForCaseClass
import sttp.tapir.json.circe.TapirJsonCirce
import sttp.tapir.{DecodeResult, PublicEndpoint, Tapir}
import yuu.Command
import yuu.implicits._

object CatImageCmd extends Command with LazyLogging {

  private object CatApi extends Tapir with TapirJsonCirce with AutoDerivation {
    case class CatResponse(id: String, url: String, width: Int, height: Int)

    val baseUri = uri"https://api.thecatapi.com"

    val search: PublicEndpoint[Unit, Unit, Seq[CatResponse], Any] = endpoint.get
      .in("v1" / "images" / "search")
      .out(jsonBody[Seq[CatResponse]])

    def apply(): IO[Either[String, String]] = {
      ArmeriaCatsBackend.resource[IO]()
        .use { backend =>
          SttpClientInterpreter()
            .toRequest(search, Some(baseUri))
            .apply()
            .send(backend)
        }
        .map { res =>
          (res.code, res.body) match {
            case (x, DecodeResult.Value(Right(Seq(body)))) if x.isSuccess =>
              Right(body.url)
            case _ =>
              Left("Cat API responded with error")
          }
        }
    }
  }

  override val command: ApplicationCommandRequest =
    ApplicationCommandRequest.builder()
      .name("cat")
      .description("Shows a cat image")
      .build()

  override def handle(event: ChatInputInteractionEvent): fs2.Stream[IO, Void] = {
    Stream.eval(CatApi())
      .flatMap {
        case Right(value) => event.reply(value).asStream()
        case Left(error)  =>
          logger.error(s"Error fetching cat image: $error")
          Stream[IO, Void]()
      }
  }

}
