package yuu.command

import cats.effect.IO
import com.typesafe.scalalogging.LazyLogging
import discord4j.core.`object`.command.ApplicationCommandOption
import discord4j.core.event.domain.interaction.ChatInputInteractionEvent
import discord4j.discordjson.json.{ApplicationCommandOptionChoiceData, ApplicationCommandOptionData, ApplicationCommandRequest}
import fs2.Stream
import io.circe.generic.AutoDerivation
import sttp.client3._
import sttp.client3.armeria.cats.ArmeriaCatsBackend
import sttp.tapir._
import sttp.tapir.client.sttp.SttpClientInterpreter
import sttp.tapir.generic.auto.schemaForCaseClass
import sttp.tapir.json.circe.TapirJsonCirce
import yuu.Command
import yuu.implicits._

import scala.jdk.CollectionConverters._

object AnimeReactionCmd extends Command with LazyLogging {

  private val Reactions: Seq[ApplicationCommandOptionChoiceData] = Seq(
    "bite",
    "blush",
    "clap",
    "confused",
    "cry",
    "facepalm",
    "headbang",
    "hug",
    "laugh",
    "lick",
    "mad",
    "nervous",
    "no",
    "nom",
    "peek",
    "pout",
    "shrug",
    "sigh",
    "sip",
    "slap",
    "sleep",
    "stare",
    "thumbsup",
    "wave",
    "woah")
    .map { r =>
      ApplicationCommandOptionChoiceData.builder()
        .name(r)
        .value(r)
        .build()
    }

  override val command: ApplicationCommandRequest =
    ApplicationCommandRequest.builder()
      .name("reaction")
      .description("Finds a gif with the appropriate reaction")
      .addOption {
        ApplicationCommandOptionData.builder()
          .name("type")
          .description("Type of reaction")
          .`type`(ApplicationCommandOption.Type.STRING.getValue)
          .choices(Reactions.asJava)
          .required(true)
          .build()
      }
      .build()

  override def handle(event: ChatInputInteractionEvent): Stream[IO, Void] = {
    val reaction = event.getOption("type").get().getValue.get().asString()
    Stream.eval(OtakuGifsApi(reaction))
      .flatMap {
        case Right(value) => event.reply(value).asStream()
        case Left(error)  =>
          logger.error(s"Error fetching gif: $error")
          Stream[IO, Void]()
      }
  }

  private object OtakuGifsApi extends Tapir with TapirJsonCirce with AutoDerivation {
    private val gifEndpoint = endpoint.get
      .in("gif")
      .in(query[String]("reaction"))
      .out(jsonBody[GifResponse])

    private val baseUri = uri"https://api.otakugifs.xyz"

    case class GifResponse(url: String)

    def apply(reaction: String): IO[Either[String, String]] = {
      ArmeriaCatsBackend.resource[IO]()
        .use { backend =>
          SttpClientInterpreter()
            .toRequest(gifEndpoint, Some(baseUri))
            .apply(reaction)
            .send(backend)
        }
        .map { res =>
          res.body match {
            case DecodeResult.Value(Right(v)) => Right(v.url)
            case _                            => Left("Gif API returned error")
          }
        }
    }
  }

}
