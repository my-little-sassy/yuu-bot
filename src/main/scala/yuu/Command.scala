package yuu

import cats.effect.IO
import fs2.Stream
import discord4j.core.event.domain.interaction.ChatInputInteractionEvent
import discord4j.discordjson.json.ApplicationCommandRequest

trait Command {

  val command: ApplicationCommandRequest

  def handle(event: ChatInputInteractionEvent): Stream[IO, Void]

}
