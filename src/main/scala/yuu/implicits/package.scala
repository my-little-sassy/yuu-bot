package yuu

import cats.effect.IO
import fs2.Stream
import fs2.interop.reactivestreams._
import org.reactivestreams.Publisher

package object implicits {

  implicit class PublisherOpsIO[A](publisher: Publisher[A]) {
    def asStream(bufferSize: Int = 1): Stream[IO, A] = fromPublisher[IO, A](publisher, bufferSize)
  }

}
