package yuu.command

import cats.effect.IO
import discord4j.core.event.domain.interaction.ChatInputInteractionEvent
import discord4j.discordjson.json.ApplicationCommandRequest
import yuu.Command
import yuu.implicits._

object IntroCmd extends Command {
  override val command: ApplicationCommandRequest =
    ApplicationCommandRequest.builder()
      .name("introduction")
      .description("It's only polite to introduce yourself")
      .build()

  override def handle(event: ChatInputInteractionEvent): fs2.Stream[IO, Void] = {
    event.reply(
      s"""おハロー！
         |My name is Yui, I'm a new bot here. I'm still not sure what I'm supposed to do, but let's do it anyway!
         |Oh I know, should I show you my cats? Type "/" and check in the commands!
         |よろしくね~*""".stripMargin).asStream()
  }
}
