import Dependencies._
import sbtassembly.AssemblyKeys.assembly
import sbtassembly.{MergeStrategy, PathList}

ThisBuild / scalaVersion     := "2.13.8"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "yuu"
ThisBuild / organizationName := "yuu"

lazy val root = (project in file("."))
  .settings(
    name := "yuu-bot",
    libraryDependencies ++= cats ++
      discord4j ++
      httpClient ++
      logging ++
      pureConfig,

    assembly / assemblyOutputPath := target.value / "jarfiles" / s"${name.value}.jar",
    assembly / assemblyMergeStrategy := {
      case PathList("META-INF", "io.netty.versions.properties")          => MergeStrategy.first
      case PathList("META-INF", "jandex.idx")                            => MergeStrategy.last
      case PathList("reactor", "core", "scheduler", "NonBlocking.class") => MergeStrategy.last
      case x if x endsWith "module-info.class"                           => MergeStrategy.discard
      case x =>
        val oldStrategy = (assembly / assemblyMergeStrategy).value
        oldStrategy(x)
    }
  )
