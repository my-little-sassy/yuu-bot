import sbt._

object Dependencies {
  private val PureConfigVersion = "0.17.2"
  private val TapirVersion      = "1.2.3"

  lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.2.11"

  lazy val discord4j = Seq(
    "com.discord4j" % "discord4j-core" % "3.2.3"
  )

  lazy val cats = Seq(
    "org.typelevel" %% "cats-core"   % "2.9.0",
    "org.typelevel" %% "cats-effect" % "3.4.1",
    "co.fs2" %% "fs2-core"             % "3.4.0",
    "co.fs2" %% "fs2-reactive-streams" % "3.4.0"
  )

  lazy val logging = Seq(
    "ch.qos.logback"             %  "logback-classic" % "1.4.4",
    "com.typesafe.scala-logging" %% "scala-logging"   % "3.9.5"
  )

  lazy val pureConfig = Seq(
    "com.github.pureconfig" %% "pureconfig"             % PureConfigVersion,
    "com.github.pureconfig" %% "pureconfig-cats-effect" % PureConfigVersion
  )

  lazy val httpClient = Seq(
    "com.softwaremill.sttp.tapir" %% "tapir-sttp-client" % TapirVersion,
    "com.softwaremill.sttp.tapir" %% "tapir-json-circe"  % TapirVersion,
    "com.softwaremill.sttp.client3" %% "armeria-backend-cats" % "3.8.3"
  )
}
