package yuu.command

import cats.effect.IO
import discord4j.core.`object`.command.ApplicationCommandOption
import discord4j.core.event.domain.interaction.ChatInputInteractionEvent
import discord4j.discordjson.json.{ApplicationCommandOptionData, ApplicationCommandRequest}
import fs2.Stream
import yuu.Command
import yuu.implicits._

object GreetCmd extends Command {
  override val command: ApplicationCommandRequest =
    ApplicationCommandRequest.builder
      .name("greet")
      .description("Greets You")
      .addOption(ApplicationCommandOptionData.builder
        .name("name")
        .description("Your name")
        .`type`(ApplicationCommandOption.Type.STRING.getValue)
        .required(true)
        .build
      )
      .build

  override def handle(event: ChatInputInteractionEvent): Stream[IO, Void] = {
    val name = event.getOption("name").get().getValue.get().asString()
    event.reply(s"Hello, $name!")
      .asStream()
  }
}
