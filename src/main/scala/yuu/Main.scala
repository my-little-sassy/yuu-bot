package yuu

import cats.effect.{ExitCode, IO, IOApp}
import com.typesafe.scalalogging.LazyLogging
import discord4j.core.event.domain.interaction.ChatInputInteractionEvent
import discord4j.core.{DiscordClient, GatewayDiscordClient}
import discord4j.discordjson.Id
import fs2._
import pureconfig.ConfigSource
import pureconfig.generic.auto._
import pureconfig.module.catseffect.syntax._
import yuu.command._
import yuu.implicits._


object Main extends IOApp with LazyLogging {
  
  def initCommands(config: AppConfig): IO[Seq[Command]] = IO {
    Seq(
      AnimeReactionCmd,
      CatImageCmd,
      IntroCmd,
      PingCmd,
      SourceCmd(Seq(Id.of(config.discordAdminId)))
    )
  }

  def loadConfig: IO[AppConfig] = {
    ConfigSource.default.loadF[IO, AppConfig]()
  }

  def createCommands(app: GatewayDiscordClient, appId: Long, cmds: Seq[Command]): IO[Unit] = IO {
    cmds.foreach { cmd =>
      logger.info(s"Creating command ${cmd.command.name()}")
      app.getRestClient.getApplicationService
        .createGlobalApplicationCommand(appId, cmd.command)
        .block()
    }
    logger.info("Finished creating commands")
  }

  def connectToDiscord(config: AppConfig, cmds: Seq[Command]): IO[Unit] = {
    val client = DiscordClient.create(config.discordToken)

    def handleEvent(event: ChatInputInteractionEvent): Stream[IO, Void] = {
      logger.info(s"Received command ${event.getCommandName}")
      cmds.find(_.command.name() == event.getCommandName)
        .map(_.handle(event))
        .getOrElse(Stream[IO, Void]())
    }

    def errorHandler(gateway: GatewayDiscordClient)(error: Throwable): Stream[IO, Void] = {
      logger.error("Discord responsed with error", error)
      gateway.on(classOf[ChatInputInteractionEvent]).asStream()
        .flatMap(handleEvent)
        .handleErrorWith(errorHandler(gateway))
    }

    client.login().asStream()
      .flatMap { gateway =>
        for {
          appId <- gateway.getRestClient.getApplicationId.asStream()
          _     <- Stream.eval(createCommands(gateway, appId, cmds))
          event <- gateway.on(classOf[ChatInputInteractionEvent]).asStream()
          _     <- handleEvent(event).handleErrorWith(errorHandler(gateway))
        } yield {}
      }
      .compile
      .drain
  }

  override def run(args: List[String]): IO[ExitCode] = {
    for {
      conf <- loadConfig
      cmds <- initCommands(conf)
      _    <- connectToDiscord(conf, cmds)
    } yield ExitCode.Success
  }

}

case class AppConfig(
  discordToken: String,
  discordGuildId: Long,
  discordAdminId: Long
)
