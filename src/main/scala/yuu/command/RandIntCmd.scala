package yuu.command

import cats.effect.IO
import discord4j.core.`object`.command.ApplicationCommandOption
import discord4j.core.event.domain.interaction.ChatInputInteractionEvent
import discord4j.discordjson.json.{ApplicationCommandOptionData, ApplicationCommandRequest}
import fs2.Stream
import yuu.Command
import yuu.implicits._

import scala.jdk.CollectionConverters._
import scala.util.Random

object RandIntCmd extends Command {
  override val command: ApplicationCommandRequest =
    ApplicationCommandRequest.builder()
      .name("random_int")
      .description("Generates a random number")
      .addOption {
        ApplicationCommandOptionData.builder()
          .name("from")
          .description("Lowest number possible")
          .`type`(ApplicationCommandOption.Type.INTEGER.getValue)
          .required(true)
          .build
      }
      .addOption {
        ApplicationCommandOptionData.builder()
          .name("to")
          .description("Highest number possible")
          .`type`(ApplicationCommandOption.Type.INTEGER.getValue)
          .required(true)
          .build
      }
      .build

  override def handle(event: ChatInputInteractionEvent): Stream[IO, Void] = {
    val from :: to :: _ = event.getOptions.asScala.toSeq.map(_.getValue.get().asLong())
    val response =
      if (from > to) s"Cannot generate a number between $from and $to"
      else Random.between(from, to + 1).toString
    event.reply(response)
      .asStream()
  }
}
