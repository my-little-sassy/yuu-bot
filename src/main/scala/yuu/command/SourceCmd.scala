package yuu.command

import cats.effect.IO
import discord4j.core.event.domain.interaction.ChatInputInteractionEvent
import discord4j.discordjson.Id
import discord4j.discordjson.json.ApplicationCommandRequest
import yuu.Command
import yuu.implicits._

class SourceCmd(adminIds: Seq[Id]) extends Command {

  private val SourceUrl: String = "https://gitlab.com/my-little-sassy/yuu-bot"

  override val command: ApplicationCommandRequest =
    ApplicationCommandRequest.builder()
      .name("source")
      .description("Shows a link to the code repository")
      .build()

  private def byAdmin(event: ChatInputInteractionEvent): Boolean =
    Option(event.getInteraction.getMember.orElse(null))
      .map(_.getMemberData.user().id())
      .exists(adminIds.contains)

  override def handle(event: ChatInputInteractionEvent): fs2.Stream[IO, Void] = {
    val reply =
      if (byAdmin(event)) {
        s"""Check out my programming! It is amazing, isn't it?
           |$SourceUrl""".stripMargin
      }
      else {
        s"""Eeeh?! You want to peek under my cover? Um... O-okay... but only a bit!
           |$SourceUrl""".stripMargin
      }
    event.reply(reply).asStream()
  }

}

object SourceCmd {
  def apply(adminIds: Seq[Id]): SourceCmd = new SourceCmd(adminIds)
}
